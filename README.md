
# Nightwatch

UI automated testing framework powered by [Node.js](http://nodejs.org/). It uses the [Selenium WebDriver API](https://code.google.com/p/selenium/wiki/JsonWireProtocol).

[![Build Status](https://travis-ci.org/beatfactor/nightwatch.png?branch=master)](https://travis-ci.org/beatfactor/nightwatch) [![NPM version](https://badge.fury.io/js/nightwatch.png)](http://badge.fury.io/js/nightwatch) [![Coverage Status](https://coveralls.io/repos/beatfactor/nightwatch/badge.png?branch=master)](https://coveralls.io/r/beatfactor/nightwatch?branch=master)

***

#### [Homepage](http://nightwatchjs.org) | [Developer Guide](http://nightwatchjs.org/guide) | [API Reference](http://nightwatchjs.org/api)

### Selenium WebDriver standalone server
Nightwatch works with the Selenium standalone server so the first thing you need to do is download the selenium server jar file `selenium-server-standalone-2.x.x.jar` from the Selenium releases page:
**http://selenium-release.storage.googleapis.com/index.html**

### Install Nightwatch

Install Node.js and then:
```sh
npm install
```

### Run tests
The test criteria are loaded within the tests directory, to run the default setting of Nightwatch run the following command. It will spawn a firefox 
browser. 
```sh
node nightwatch.js
```
The result of the test are display within the command prompt or they are also exported into the reports/ folder.
To run nightwatch on different enviroment run the following command.

For chrome:

```sh
nightwatch --env chrome
```

For IE:

```sh
nightwatch --env ie
```


# Setting Up different Test Cases

For Dancik test , it is best to place the contents on their Destination folder within the root of the application.
Since each test is different, it is recommended to have different test cases and extend each case into the init.js file which is locates in the init/ folder

Nightwatch is build to run every test, that is inside the init folder as an individial instance.  If you wish to run seperate test that is not related to the first in a seperate instace, you may
do so by adding a new file instide the init/ directory. 

I would recommend to extend from init.js, which it will run all the test from the tests/directory.


### Discuss
In addition to [Twitter](https://twitter.com/nightwatchjs), the [Mailing List/Google Group](https://groups.google.com/forum/#!forum/nightwatchjs) is also available for Nightwatch related discussions.

### Setup Guides
Browser specific setup and usage guides along with debugging instructions can be found on the [**Wiki**](https://github.com/beatfactor/nightwatch/wiki).

### Manually Shutdown Selenium
http://localhost:4444/selenium-server/?cmd=shutDownSeleniumServer