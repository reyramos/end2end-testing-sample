var extend = require('node.extend');



module.exports = extend(true,

    //Open the browser
    {
    "Open sal.dancik.com/int/radar": function(browser) {
        var url = "http://sal.dancik.com/int/radar";
        browser
            .url(url)
            .waitForElementVisible('body', 10000)
            .pause(1000) //lets pause while we log in
    },
}

/*************************************************************
* ALL THE ADDITIONAL TEST CASES, THIS WILL RUN IN INSEQUENTIAL
* ORDER FROM THE FIRST TEST.
*************************************************************/
, require('../tests/radar')




// Close the browser
, {
    "Close Browser": function(browser) {
        browser
            .end()
    }
});






