module.exports = {

    "Click on Sales Analysis": function(browser) {
        browser
            .useXpath()
            .click("/html/body/div[1]/div[2]/div[3]/div[1]")
            .pause(1000)
    },

    "Test Date Types Ranking (Range)": function(browser) {

        var view_FromDate, view_ToDate, view_FromDate1, view_ToDate1, view_FromDate2, view_ToDate2;

        browser.useXpath()
            .click('//*[@id="span_SAType_1"]') //Ranking (Range)
            .useCss() //search by css
            .assert.containsText('#SA_Date1 > td.SA_td', 'Date Range')
            .pause(500)
            .click('#view_FromDate')
            .pause(500)
            .click('.dws-Calendar-PreviousYear') //set the year a few click back
            .click('.dws-Calendar-PreviousYear') //set the year a few click back
            .click('.dws-Calendar-Table > tbody > tr:nth-child(3) > td:nth-child(3)')
            .pause(500)
            .getValue('#view_FromDate', function(result) {
                var date = result.value.split('/')
                view_FromDate = date[2] + "" + date[0] + "" + date[1];
            })
            .getValue('#parm_FromDate', function(result) {
                this.assert.equal(result.value, view_FromDate);
            })
            .click('#view_ToDate')
            .pause(500)
            .click('.dws-Calendar-Table > tbody > tr:nth-child(2) > td:nth-child(3)')
            .pause(500)
            .getValue('#view_ToDate', function(result) {
                var date = result.value.split('/')
                view_ToDate = date[2] + "" + date[0] + "" + date[1];
            })
            .getValue('#parm_ToDate', function(result) {
                this.assert.equal(result.value, view_ToDate);

            })
            .pause(500)

    },


    "Test Date Types Comparison (Range)": function(browser) {

        var view_FromDate1, view_ToDate1, view_FromDate2, view_ToDate2;

        browser.useXpath()
            .click('//*[@id="span_SAType_4"]') //Ranking (Range)
            .useCss() //search by css
            .assert.containsText('#SA_Date4a > td.SA_td', 'Date Range (This)')
            .pause(500)

        //first date from left
        .click('#view_FromDate1')
            .pause(500)
            .click('.dws-Calendar-PreviousYear') //set the year a few click back
            .click('.dws-Calendar-Table > tbody > tr:nth-child(3) > td:nth-child(3)')
            .pause(500)
            .getValue('#view_FromDate1', function(result) {
                var date = result.value.split('/')
                view_FromDate = date[2] + "" + date[0] + "" + date[1];
            })
            .getValue('#parm_FromDate1', function(result) {
                this.assert.equal(result.value, view_FromDate);
            })

        //second date from left
        .click('#view_ToDate1')
            .pause(500)
            .click('.dws-Calendar-Table > tbody > tr:nth-child(2) > td:nth-child(3)')
            .pause(500)
            .getValue('#view_ToDate1', function(result) {
                var date = result.value.split('/')
                view_ToDate = date[2] + "" + date[0] + "" + date[1];
            })
            .getValue('#parm_ToDate1', function(result) {
                this.assert.equal(result.value, view_ToDate);

            })
            .pause(500)

        //second date from left
        .click('#view_FromDate2')
            .pause(500)
            .click('.dws-Calendar-PreviousYear') //set the year a few click back
            .click('.dws-Calendar-PreviousYear') //set the year a few click back
            .click('.dws-Calendar-Table > tbody > tr:nth-child(3) > td:nth-child(3)')
            .pause(500)
            .getValue('#view_FromDate2', function(result) {
                var date = result.value.split('/')
                view_FromDate = date[2] + "" + date[0] + "" + date[1];
            })
            .getValue('#parm_FromDate2', function(result) {
                this.assert.equal(result.value, view_FromDate);
            })

        //second date from left
        .click('#view_ToDate2')
            .pause(500)
            .click('.dws-Calendar-PreviousYear') //set the year a few click back
            .click('.dws-Calendar-Table > tbody > tr:nth-child(2) > td:nth-child(3)')
            .pause(500)
            .getValue('#view_ToDate2', function(result) {
                var date = result.value.split('/')
                view_ToDate = date[2] + "" + date[0] + "" + date[1];
            })
            .getValue('#parm_ToDate2', function(result) {
                this.assert.equal(result.value, view_ToDate);

            })
            .pause(500)


    },





    //     "Open Saved Filters": function(browser) {
    //     browser
    //         .useXpath()
    //         .click('//*[@id="editTab0"]')
    //         .pause(1000)
    //         .click('//*[@id="SavedParms_ListingArea"]/div/div[1]/div[1]/div[2]') //click on no Category
    //         .pause(500)
    //         .click('//*[@id="CatTBody_0"]/div[1]/div[2]') //click on test 
    //         .pause(500)
    //         .useCss()//search by css
    //         .verify.visible('fieldset')
    //         .end()
    // }

}; //end of module.exports
